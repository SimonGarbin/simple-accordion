jQuery(document).ready(function ($) {

    $('.simple-accordion:not(.always-open) .simple-accordion-title-wrap').on(
            'click touchstart', function () {
                var titleWrap = $(this);
                var parent = titleWrap.parent();
                var content = titleWrap.next();

                parent.toggleClass('is-closed');
                if (parent.hasClass('is-closed')) {
                    content.slideUp(300);
                } else {
                    content.slideDown(300);
                }
            });
});