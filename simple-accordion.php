<?php
/**
 * Plugin Name: Simple Accordion
 * Plugin URI: https://gitlab.com/SimonGarbin/simple-accordion
 * Description: ...
 * Version: 1.0.0
 * Requires at least: 6.2
 * Requires PHP: 8.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: simple-accordion
 * Domain Path: /languages

  Copyright 2023 Simon Garbin
  Simple Accordion is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Simple Accordion is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Simple Accordion. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

function simple_accordion_translation_dummy() {
    $plugin_description = __('...', 'simple-accordion');
}

add_action(
        hook_name: 'init',
        callback: 'simple_accordion_load_textdomain'
);

function simple_accordion_load_textdomain() {
    load_plugin_textdomain(
            domain: 'simple-accordion',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'simple_accordion_settings_link'
);

function simple_accordion_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=simple_accordion');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'simple-accordion')
            . '</a>';
    $links[] = $link;

    return $links;
}

add_action(
        hook_name: 'init',
        callback: 'simple_accordion_register_block'
);

function simple_accordion_register_block() {

    $asset_file = include(plugin_dir_path(__FILE__) . '/block/block.asset.php');
    
    $editor_style_handle = 'simple-accordion-editor-style';
    $style_handle = 'simple-accordion-style';
    $script_handle = 'simple-accordion-editor-script';

    wp_register_style(
            handle: $editor_style_handle,
            src: plugins_url('block/editor.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );
    
    wp_register_style(
            handle: $style_handle,
            src: plugins_url('block/style.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );
    
    wp_register_script(
            handle: $script_handle,
            src: plugins_url('block/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'simple-accordion',
            path: plugin_dir_path(__FILE__) . 'languages/'
    );
        
    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/block/',
            args: array(
                'api_version' => 2,
                'render_callback' => 'simple_accordion_render_callback',
            )
    );
}

function simple_accordion_render_callback($attributes, $content, $block) {

    if (!isset($attributes['title']) || !$attributes['title']) {
        return '';
    }

    // Anchor and classes
    $anchor = '';

    if (isset($attributes['anchor']) && $attributes['anchor']) {
        $anchor .= sprintf(' id="%s"', esc_attr($attributes['anchor']));
    }

    $classes = array(
        'simple-accordion',
    );

    if (!isset($attributes['openByDefault']) || !$attributes['openByDefault']) {
        $classes[] = 'is-closed';
    }

    if (isset($attributes['alwaysOpen']) && $attributes['alwaysOpen']) {
        $classes[] = 'always-open';
    }

    if (isset($attributes['bordered']) && $attributes['bordered']) {
        $classes[] = 'has-border';
    }

    if (isset($attributes['className']) && $attributes['className']) {
        $classes[] = $attributes['className'];
    }

    // Gather styles from block attributes
    $styles = array(
        'block' => array(),
        'title-wrap' => array(),
        'title' => array(),
        'content' => array(),
    );

    if (isset($attributes['width']) && $attributes['width']) {
        $styles['block'][] = 'width:' . esc_attr($attributes['width']);
    }

    if (isset($attributes['alignment']) && $attributes['alignment']) {
        if ($attributes['alignment'] === 'center') {
            $styles['block'][] = 'margin-left:auto';
            $styles['block'][] = 'margin-right:auto';
        } else if ($attributes['alignment'] === 'right') {
            $styles['block'][] = 'margin-left:auto';
            $styles['block'][] = 'margin-right:0';
        }
    }

    if (isset($attributes['bordered']) && $attributes['bordered']) {
        if (isset($attributes['titleBgColor']) && $attributes['titleBgColor']) {
            $styles['content'][] = 'border-color:' . esc_attr($attributes['titleBgColor']);
        }
    }

    if (isset($attributes['borderRadius']) && $attributes['borderRadius']) {
        $styles['block'][] = 'border-radius:' . esc_attr($attributes['borderRadius']);
        $styles['content'][] = 'border-bottom-left-radius:' . esc_attr($attributes['borderRadius']);
        $styles['content'][] = 'border-bottom-right-radius:' . esc_attr($attributes['borderRadius']);
    }

    if (isset($attributes['titleFontColor']) && $attributes['titleFontColor']) {
        $styles['title'][] = 'color:' . esc_attr($attributes['titleFontColor']);
    }

    if (isset($attributes['titleBgColor']) && $attributes['titleBgColor']) {
        $styles['title-wrap'][] = 'background:' . esc_attr($attributes['titleBgColor']);
    }

    if (isset($attributes['contentFontColor']) && $attributes['contentFontColor']) {
        $styles['content'][] = 'color:' . esc_attr($attributes['contentFontColor']);
    }

    if (isset($attributes['contentBgColor']) && $attributes['contentBgColor']) {
        $styles['content'][] = 'background:' . esc_attr($attributes['contentBgColor']);
    }

    if (isset($attributes['openByDefault']) && !$attributes['openByDefault']) {
        $styles['content'][] = 'display:none';
    }

    // Combine CSS properties to style attribute
    foreach ($styles as $key => $values) {
        if (!empty($values)) {
            $styles[$key] = sprintf(' style="%s;"', implode(';', $values));
        } else {
            $styles[$key] = '';
        }
    }

    // Generate HTML
    $output = sprintf(
            '<div %1$sclass="%2$s"%3$s>',
            $anchor,
            esc_attr(implode(' ', $classes)),
            $styles['block'],
    );

    // Title
    $no_pointer = '';
    if (isset($attributes['alwaysOpen']) && $attributes['alwaysOpen']) {
        $no_pointer = ' no-pointer';
    }
    $output .= sprintf(
            '<div class="simple-accordion-title-wrap%2$s"%1$s>',
            $styles['title-wrap'],
            $no_pointer,
    );

    $title_tag = 'span';
    if (isset($attributes['titleTag']) && $attributes['titleTag']) {
        $title_tag = $attributes['titleTag'];
    }
    $output .= sprintf(
            '<%2$s class="simple-accordion-title"%3$s>%1$s</%2$s>',
            $attributes['title'],
            $title_tag,
            $styles['title']
    );

    if (!isset($attributes['alwaysOpen']) || !$attributes['alwaysOpen']) {
        $output .= sprintf(
                        '<div class="simple-accordion-arrow"%s>',
                        $styles['title']
                )
                . '<i class="fa fa-angle-down fa-sm"></i>'
                . '</div>';
    }
    $output .= '</div>';

    // Content
    $output .= sprintf(
            '<div class="simple-accordion-content"%s>',
            $styles['content']
    );
    foreach ($block->inner_blocks as $inner_block) {
        $output .= $inner_block->render();
    }
    $output .= '</div>';

    $output .= '</div>';

    return $output;
}

/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'simple_accordion_font_awesome'
);

function simple_accordion_font_awesome() {

    wp_register_style(
            handle: 'font-awesome-v6.4.0',
            src: 'https://use.fontawesome.com/releases/v6.4.0/css/all.css',
            deps: array(),
            ver: false,
            media: 'all'
    );
}

add_action(
        hook_name: 'init',
        callback: 'simple_accordion_toggle_script'
);

function simple_accordion_toggle_script() {

    $handle = 'simple-accordion-toggle-script';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/toggle.js', __FILE__),
            deps: array('jquery'),
            ver: false,
            in_footer: true
    );
}
