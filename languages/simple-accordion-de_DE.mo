��          �      L      �     �  	   �     �  
   �     �     �     �     
          #     4     9     ?  	   L     V     \     c     h  �  n     J     N     Z     f  	   r     |  	   �     �     �     �     �     �     �  	   �     �     �                                      
                               	                                         ... Alignment Always open Background Border Content colors Corner radius Open by default Settings Simple Accordion Text Title Title colors Title tag Width center left right Project-Id-Version: Simple Accordion
PO-Revision-Date: 2023-10-05 19:31+0200
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: block/block.js
X-Poedit-SearchPath-1: simple-accordion.php
 ... Ausrichtung Immer offen Hintergrund Randlinie Farben des Inhalts Eckradius Standardmäßig offen Einstellungen Einfaches Akkordeon Text Titel Farben des Titels Titel-Tag Breite mittig links rechts 