(function (blocks, element, blockEditor, components, i18n) {

    var registerBlockType = blocks.registerBlockType;
    var el = element.createElement;
    var __ = i18n.__;

    var useBlockProps = blockEditor.useBlockProps;
    var InspectorControls = blockEditor.InspectorControls;
    var RichText = blockEditor.RichText;
    var InnerBlocks = blockEditor.InnerBlocks;
    var PanelColorSettings = blockEditor.PanelColorSettings;

    var ToggleControl = components.ToggleControl;
    var SelectControl = components.SelectControl;
    var PanelBody = components.PanelBody;
    var UnitControl = components.__experimentalUnitControl;

    registerBlockType('simple-accordion/block', {
        apiVersion: 2,
        title: __('Simple Accordion', 'simple-accordion'),
        category: 'widgets',
        icon: 'editor-ul',
        supports: {
            anchor: true
        },
        attributes: {
            title: {
                type: 'string',
                default: null
            },
            titleTag: {
                type: 'string',
                default: ''
            },
            openByDefault: {
                type: 'boolean',
                default: false
            },
            alwaysOpen: {
                type: 'boolean',
                default: false
            },
            width: {
                type: 'string',
                default: null
            },
            alignment: {
                type: 'string',
                default: 'left'
            },
            bordered: {
                type: 'boolean',
                default: false
            },
            borderRadius: {
                type: 'string',
                default: null
            },
            titleFontColor: {
                type: 'string',
                default: null
            },
            titleBgColor: {
                type: 'string',
                default: null
            },
            contentFontColor: {
                type: 'string',
                default: null
            },
            contentBgColor: {
                type: 'string',
                default: null
            },
            anchor: {
                type: 'string',
                default: null
            }
        },
        edit: function (props) {
            var attributes = props.attributes;
            var setAttributes = props.setAttributes;

            function onChangeTitle(newTitle) {
                setAttributes({title: newTitle || null});
            }

            function onChangeTitleTag(newTitleTag) {
                setAttributes({titleTag: newTitleTag});
            }

            function onChangeWidth(newWidth) {
                setAttributes({width: newWidth || null});
            }

            function onChangeBorderRadius(newBorderRadius) {
                setAttributes({borderRadius: newBorderRadius || null});
            }

            function onChangeTitleFontColor(newTitleFontColor) {
                setAttributes({titleFontColor: newTitleFontColor || null});
            }

            function onChangeTitleBgColor(newTitleBgColor) {
                setAttributes({titleBgColor: newTitleBgColor || null});
            }

            function onChangeContentFontColor(newContentFontColor) {
                setAttributes({contentFontColor: newContentFontColor || null});
            }

            function onChangeContentBgColor(newContentBgColor) {
                setAttributes({contentBgColor: newContentBgColor || null});
            }

            function onChangeOpenByDefault(newOpenByDefault) {
                if (attributes.alwaysOpen) {
                    setAttributes({openByDefault: true});
                } else {
                    setAttributes({openByDefault: newOpenByDefault});
                }
            }

            function onChangeAlwaysOpen(newAlwaysOpen) {
                setAttributes({alwaysOpen: newAlwaysOpen});
                if (newAlwaysOpen) {
                    setAttributes({openByDefault: true});
                }
            }

            function onChangeBordered(newBordered) {
                setAttributes({bordered: newBordered});
            }

            function onChangeAlignment(newAlignment) {
                setAttributes({alignment: newAlignment});
            }

            function getMargin() {
                if (attributes.alignment === 'left') {
                    return '1em auto 1em 0';
                } else if (attributes.alignment === 'center') {
                    return '1em auto';
                } else if (attributes.alignment === 'right') {
                    return '1em 0 1em auto';
                }
            }

            return el('div', useBlockProps(),
                    el('div', {
                        id: attributes.anchor,
                        className: 'simple-accordion',
                        style: {
                            'width': attributes.width,
                            'margin': getMargin(),
                            'border-radius': attributes.borderRadius
                        }
                    },
                            el('div', {
                                className: 'simple-accordion-title-wrap',
                                style: {
                                    'background': attributes.titleBgColor,
                                    'border-color': attributes.titleBgColor
                                            ? attributes.titleBgColor : null
                                }
                            },
                                    el(RichText, {
                                        tagName: 'span',
                                        className: 'simple-accordion-title',
                                        value: attributes.title,
                                        placeholder: __('Title', 'simple-accordion'),
                                        onChange: onChangeTitle,
                                        style: {
                                            'color': attributes.titleFontColor
                                        }
                                    }),
                                    el('div', {
                                        className: 'simple-accordion-arrow',
                                        style: {
                                            'color': attributes.titleFontColor
                                        }
                                    },
                                            el('i', {className: 'fa fa-angle-down fa-sm'})
                                            )
                                    ),
                            el('div', {
                                className: 'simple-accordion-content',
                                style: {
                                    'color': attributes.contentFontColor,
                                    'background': attributes.contentBgColor,
                                    'border-color': attributes.titleBgColor
                                            ? attributes.titleBgColor : null,
                                    'border-style': attributes.bordered ? 'none solid solid' : null,
                                    'border-bottom-left-radius': attributes.borderRadius,
                                    'border-bottom-right-radius': attributes.borderRadius
                                }
                            },
                                    el(InnerBlocks, {orientation: 'vertical'})
                                    ),
                            ),
                    el(InspectorControls, {group: 'settings'},
                            el(PanelBody, {},
                                    el(SelectControl, {
                                        label: __('Title tag', 'simple-accordion'),
                                        labelPosition: 'side',
                                        className: 'simple-accordion-input-control',
                                        value: attributes.titleTag,
                                        onChange: onChangeTitleTag,
                                        options: [
                                            {value: 'span', label: 'span', selected: true},
                                            {value: 'p', label: 'p'},
                                            {value: 'h1', label: 'h1'},
                                            {value: 'h2', label: 'h2'},
                                            {value: 'h3', label: 'h3'},
                                            {value: 'h4', label: 'h4'}
                                        ]
                                    }),
                                    el(ToggleControl, {
                                        checked: attributes.openByDefault,
                                        label: __('Open by default', 'simple-accordion'),
                                        onChange: onChangeOpenByDefault,
                                        disabled: attributes.alwaysOpen
                                    }),
                                    el(ToggleControl, {
                                        checked: attributes.alwaysOpen,
                                        label: __('Always open', 'simple-accordion'),
                                        onChange: onChangeAlwaysOpen
                                    }),
                                    )
                            ),
                    el(InspectorControls, {group: 'styles'},
                            el(PanelBody, {},
                                    el(UnitControl, {
                                        label: __('Width', 'simple-accordion'),
                                        labelPosition: 'side',
                                        className: 'simple-accordion-input-control',
                                        value: attributes.width,
                                        onChange: onChangeWidth
                                    }),
                                    el(SelectControl, {
                                        label: __('Alignment', 'simple-accordion'),
                                        labelPosition: 'side',
                                        className: 'simple-accordion-input-control',
                                        value: attributes.alignment,
                                        onChange: onChangeAlignment,
                                        options: [
                                            {
                                                value: 'left',
                                                label: __('left', 'simple-accordion'),
                                                selected: true
                                            },
                                            {
                                                value: 'center',
                                                label: __('center', 'simple-accordion')
                                            },
                                            {
                                                value: 'right',
                                                label: __('right', 'simple-accordion')
                                            }
                                        ]
                                    }),
                                    el(UnitControl, {
                                        label: __('Corner radius', 'simple-accordion'),
                                        labelPosition: 'side',
                                        className: 'simple-accordion-input-control',
                                        value: attributes.borderRadius,
                                        onChange: onChangeBorderRadius
                                    }),
                                    el(ToggleControl, {
                                        checked: attributes.bordered,
                                        label: __('Border', 'simple-accordion'),
                                        onChange: onChangeBordered,
                                        id: 'hallo'
                                    })
                                    ),
                            el(PanelColorSettings, {
                                title: __('Title colors', 'simple-accordion'),
                                colorSettings: [
                                    {
                                        value: attributes.titleFontColor,
                                        onChange: onChangeTitleFontColor,
                                        label: __('Text', 'simple-accordion')
                                    },
                                    {
                                        value: attributes.titleBgColor,
                                        onChange: onChangeTitleBgColor,
                                        label: __('Background', 'simple-accordion')
                                    }
                                ]
                            }),
                            el(PanelColorSettings, {
                                title: __('Content colors', 'simple-accordion'),
                                colorSettings: [
                                    {
                                        value: attributes.contentFontColor,
                                        onChange: onChangeContentFontColor,
                                        label: __('Text', 'simple-accordion')
                                    },
                                    {
                                        value: attributes.contentBgColor,
                                        onChange: onChangeContentBgColor,
                                        label: __('Background', 'simple-accordion')
                                    }
                                ]
                            })
                            )
                    );
        },
        save: function (props) {
            return el(InnerBlocks.Content, useBlockProps.save());
        }
    });
})(
        window.wp.blocks,
        window.wp.element,
        window.wp.blockEditor,
        window.wp.components,
        window.wp.i18n
        );